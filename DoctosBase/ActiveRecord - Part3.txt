acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 7199
Loading development environment (Rails 5.2.3)
irb(main):001:0> htwn = Hometown.find(1)
  Hometown Load (0.6ms)  SELECT  "hometowns".* FROM "hometowns" WHERE "hometowns"."id" = ? LIMIT ?  [["id", 1], ["LIMIT", 1]]
=> #<Hometown id: 1, state_city_key: 1001, state_name: "AGUASCALIENTES", city_name: "AGUASCALIENTES", created_at: "2019-08-23 23:18:02", updated_at: "2019-08-23 23:18:02", competitors_count: 0>
irb(main):002:0> htwn.new_record?
=> false
irb(main):003:0> pago = Payment.find(3)
  Payment Load (0.4ms)  SELECT  "payments".* FROM "payments" WHERE "payments"."id" = ? LIMIT ?  [["id", 3], ["LIMIT", 1]]
=> #<Payment id: 3, method: "TRANSFERENCIA", status: "PAGADO", auth_number: "166929-4", amount: 350, comments: "", created_at: "2019-08-23 23:18:02", updated_at: "2019-08-23 23:18:02", competitor_id: 3>
irb(main):004:0> pago = Payment.find_by_auth_number("166929-4")
  Payment Load (0.6ms)  SELECT  "payments".* FROM "payments" WHERE "payments"."auth_number" = ? LIMIT ?  [["auth_number", "166929-4"], ["LIMIT", 1]]
=> #<Payment id: 3, method: "TRANSFERENCIA", status: "PAGADO", auth_number: "166929-4", amount: 350, comments: "", created_at: "2019-08-23 23:18:02", updated_at: "2019-08-23 23:18:02", competitor_id: 3>
irb(main):005:0> pago.competitor
  Competitor Load (0.8ms)  SELECT  "competitors".* FROM "competitors" WHERE "competitors"."id" = ? LIMIT ?  [["id", 3], ["LIMIT", 1]]
=> nil
irb(main):006:0> pago = Payment.find_by_auth_number("666")
  Payment Load (0.4ms)  SELECT  "payments".* FROM "payments" WHERE "payments"."auth_number" = ? LIMIT ?  [["auth_number", "666"], ["LIMIT", 1]]
=> #<Payment id: 7, method: "TRANSFERENCIA", status: "PAGADO", auth_number: "666", amount: 380, comments: "Pago completo", created_at: "2019-08-27 22:48:08", updated_at: "2019-08-27 22:48:08", competitor_id: 24>
irb(main):007:0> pago.competitor
  Competitor Load (0.3ms)  SELECT  "competitors".* FROM "competitors" WHERE "competitors"."id" = ? LIMIT ?  [["id", 24], ["LIMIT", 1]]
=> #<Competitor id: 24, number: 118, first_lname: "Enrique", second_lname: "Escobar", names: "Gilberto", b_day: 19850919, gender: "M", e_mail: "eroz_g21@hotmail.com", blood_type: "N/A", phone: 476, allergies: "N/A", emergency_c_name: "Mary (esposa)", emergency_c_phone: 476, team: "Krudos Bike", adult_tickets: 1, children_tickets: 1, amount_extra_tickets: 0, route_to_road: "Guacamayitas (45 km)", responsive_sent: true, responsive_received: nil, signature: " ", store_id: 1, hometown_id: 23, kind_id: 1, size_id: 3, created_at: "2019-08-26 15:25:12", updated_at: "2019-08-26 15:25:12">
irb(main):008:0> pago.competitor.names
=> "Gilberto"
irb(main):009:0> pago = Payment.find_by_auth_number("666")
  Payment Load (0.5ms)  SELECT  "payments".* FROM "payments" WHERE "payments"."auth_number" = ? LIMIT ?  [["auth_number", "666"], ["LIMIT", 1]]
=> #<Payment id: 7, method: "TRANSFERENCIA", status: "PAGADO", auth_number: "666", amount: 380, comments: "Pago completo", created_at: "2019-08-27 22:48:08", updated_at: "2019-08-27 22:48:08", competitor_id: 25>
irb(main):010:0> pago.competitor.name
  Competitor Load (0.3ms)  SELECT  "competitors".* FROM "competitors" WHERE "competitors"."id" = ? LIMIT ?  [["id", 25], ["LIMIT", 1]]
Traceback (most recent call last):
        1: from (irb):10
NoMethodError (undefined method `name' for #<Competitor:0x0000561c13810be8>)
Did you mean?  names
irb(main):011:0> pago.competitor.names
=> "Julio Esteban"
irb(main):012:0> pago.competitor.number
=> 666
irb(main):013:0> pago.competitor.e_mail
=> "dicafis@gmail.com"
irb(main):014:0> exit



acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9448
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
=> #<Payment id: nil, method: "DEPOSITO", status: "PAGADO", auth_number: "654321", amount: 380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):002:0> pago_nuevo.new?
Traceback (most recent call last):
        1: from (irb):2
NoMethodError (undefined method `new?' for #<Payment:0x0000561c125feba8>)
irb(main):003:0> pago_nuevo.new_record?
=> true
irb(main):004:0> pago_nuevo.save
   (0.4ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):005:0> errors[:comments]
Traceback (most recent call last):
        1: from (irb):5
NameError (undefined local variable or method `errors' for main:Object)
irb(main):006:0> pago_nuevo.errors
=> #<ActiveModel::Errors:0x0000561c122aa178 @base=#<Payment id: nil, method: "DEPOSITO", status: "PAGADO", auth_number: "654321", amount: 380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>, @messages={:competitor=>["must exist"], :competitor_id=>["can't be blank"]}, @details={:competitor=>[{:error=>:blank}], :competitor_id=>[{:error=>:blank}]}>
irb(main):007:0> pago_nuevo.errors.on(:competitor_id)
Traceback (most recent call last):
        1: from (irb):7
NoMethodError (undefined method `on' for #<ActiveModel::Errors:0x0000561c122aa178>)
irb(main):008:0> pago_nuevo.errors.[:competitor_id]
Traceback (most recent call last):
SyntaxError ((irb):8: syntax error, unexpected '[', expecting '(')
pago_nuevo.errors.[:competitor_id]
                  ^
irb(main):009:0> pago_nuevo.errors[:competitor_id]
=> ["can't be blank"]
irb(main):010:0> pago_nuevo.errors.each do |e| p e end
:competitor
:competitor_id
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):011:0> pago_nuevo.errors.each do |k, v| p k + v end
Traceback (most recent call last):
        2: from (irb):11
        1: from (irb):11:in `block in irb_binding'
NoMethodError (undefined method `+' for :competitor:Symbol)
irb(main):012:0> pago_nuevo.errors.each do |k, v| p k.to_s + v end
"competitormust exist"
"competitor_idcan't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):013:0> pago_nuevo.errors.each do |k, v| p k.to_s " " + v end
Traceback (most recent call last):
        3: from (irb):13
        2: from (irb):13:in `block in irb_binding'
        1: from (irb):13:in `to_s'
ArgumentError (wrong number of arguments (given 1, expected 0))
irb(main):014:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):015:0> 







irb(main):015:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):016:0> pago_nuevo.save
   (0.1ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):017:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
"method can't be blank"
"status can't be blank"
"auth_number can't be blank"
"amount can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"], :method=>["can't be blank"], :status=>["can't be blank"], :auth_number=>["can't be blank"], :amount=>["can't be blank"]}
irb(main):018:0> 







acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9207
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios)
irb(main):002:1" ")
Traceback (most recent call last):
        1: from (irb):1
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/payment.rb:6: syntax error, unexpected =>, expecting keyword_end)
...                    :message => 'debe haber un competidor li...
...                             ^~
irb(main):003:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9283
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
Traceback (most recent call last):
        3: from (irb):1
        2: from app/models/payment.rb:1:in `<top (required)>'
        1: from app/models/payment.rb:4:in `<class:Payment>'
ArgumentError (An object with the method #include? or a proc, lambda or symbol is required, and must be supplied as the :in (or :within) option of the configuration hash)
irb(main):002:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9345
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
Traceback (most recent call last):
        1: from (irb):1
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/payment.rb:5: syntax error, unexpected =>, expecting keyword_end)
                        :on => :create
                            ^~
irb(main):002:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9390
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
Traceback (most recent call last):
        1: from (irb):1
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/payment.rb:5: syntax error, unexpected ':', expecting keyword_end)
                        on: :create
                          ^
irb(main):002:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 9448
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
=> #<Payment id: nil, method: "DEPOSITO", status: "PAGADO", auth_number: "654321", amount: 380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):002:0> pago_nuevo.new?
Traceback (most recent call last):
        1: from (irb):2
NoMethodError (undefined method `new?' for #<Payment:0x0000561c125feba8>)
irb(main):003:0> pago_nuevo.new_record?
=> true
irb(main):004:0> pago_nuevo.save
   (0.4ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):005:0> errors[:comments]
Traceback (most recent call last):
        1: from (irb):5
NameError (undefined local variable or method `errors' for main:Object)
irb(main):006:0> pago_nuevo.errors
=> #<ActiveModel::Errors:0x0000561c122aa178 @base=#<Payment id: nil, method: "DEPOSITO", status: "PAGADO", auth_number: "654321", amount: 380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>, @messages={:competitor=>["must exist"], :competitor_id=>["can't be blank"]}, @details={:competitor=>[{:error=>:blank}], :competitor_id=>[{:error=>:blank}]}>
irb(main):007:0> pago_nuevo.errors.on(:competitor_id)
Traceback (most recent call last):
        1: from (irb):7
NoMethodError (undefined method `on' for #<ActiveModel::Errors:0x0000561c122aa178>)
irb(main):008:0> pago_nuevo.errors.[:competitor_id]
Traceback (most recent call last):
SyntaxError ((irb):8: syntax error, unexpected '[', expecting '(')
pago_nuevo.errors.[:competitor_id]
                  ^
irb(main):009:0> pago_nuevo.errors[:competitor_id]
=> ["can't be blank"]
irb(main):010:0> pago_nuevo.errors.each do |e| p e end
:competitor
:competitor_id
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):011:0> pago_nuevo.errors.each do |k, v| p k + v end
Traceback (most recent call last):
        2: from (irb):11
        1: from (irb):11:in `block in irb_binding'
NoMethodError (undefined method `+' for :competitor:Symbol)
irb(main):012:0> pago_nuevo.errors.each do |k, v| p k.to_s + v end
"competitormust exist"
"competitor_idcan't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):013:0> pago_nuevo.errors.each do |k, v| p k.to_s " " + v end
Traceback (most recent call last):
        3: from (irb):13
        2: from (irb):13:in `block in irb_binding'
        1: from (irb):13:in `to_s'
ArgumentError (wrong number of arguments (given 1, expected 0))
irb(main):014:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}
irb(main):015:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):016:0> pago_nuevo.save
   (0.1ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):017:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
"method can't be blank"
"status can't be blank"
"auth_number can't be blank"
"amount can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"], :method=>["can't be blank"], :status=>["can't be blank"], :auth_number=>["can't be blank"], :amount=>["can't be blank"]}
irb(main):018:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):019:0> pago_nuevo.save
   (0.2ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):020:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
"method can't be blank"
"status can't be blank"
"auth_number can't be blank"
"amount can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"], :method=>["can't be blank"], :status=>["can't be blank"], :auth_number=>["can't be blank"], :amount=>["can't be blank"]}
irb(main):021:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 14512
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new()
Traceback (most recent call last):
        1: from (irb):1
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/payment.rb:21: syntax error, unexpected ':', expecting keyword_end)
...                       message: "Debe ser numérico"
...                              ^
irb(main):002:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 14752
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):002:0> pago_nuevo.save
   (0.2ms)  begin transaction
   (0.1ms)  rollback transaction
Traceback (most recent call last):
        1: from (irb):2
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/competitor.rb:11: syntax error, unexpected '\n', expecting =>)
...           on: :create, :update
...                               ^
/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/competitor.rb:15: syntax error, unexpected '\n', expecting =>
...           on: :create, :update
...                               ^
/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/competitor.rb:36: syntax error, unexpected ':', expecting keyword_end
                       is: 10,
                         ^
irb(main):003:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 14855
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):002:0> pago_nuevo.save
   (0.1ms)  begin transaction
   (0.1ms)  rollback transaction
Traceback (most recent call last):
        1: from (irb):2
SyntaxError (/home/acevedoj/Documents/EjerciciosRuby/active_record_guaca/app/models/competitor.rb:35: syntax error, unexpected ':', expecting keyword_end)
                       is: 10,
                         ^
irb(main):003:0> exit
acevedoj@EC-LT-285:~/Documents/EjerciciosRuby/active_record_guaca$ rails console
Running via Spring preloader in process 14880
Loading development environment (Rails 5.2.3)
irb(main):001:0> pago_nuevo = Payment.new()
=> #<Payment id: nil, method: nil, status: nil, auth_number: nil, amount: nil, comments: nil, created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):002:0> pago_nuevo.save
   (0.1ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):003:0> ^C
irb(main):003:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
"method can't be blank"
"status can't be blank"
"auth_number can't be blank"
"amount can't be blank"
"amount ¿Ingresaste el monto correcto del pago?"
"amount Debe ser numérico"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"], :method=>["can't be blank"], :status=>["can't be blank"], :auth_number=>["can't be blank"], :amount=>["can't be blank", "¿Ingresaste el monto correcto del pago?", "Debe ser numérico"]}


irb(main):004:0> pago_nuevo = Payment.new(method: "DEPOSITO", status: "PAGADO", auth_number: 654321, amount: "380", comments: "Comentarios")
=> #<Payment id: nil, method: "DEPOSITO", status: "PAGADO", auth_number: "654321", amount: 380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):005:0> pago_nuevo.save
   (0.2ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):006:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"]}



irb(main):007:0> pago_nuevo = Payment.new(method: "DEPOSITOSITOSESES", status: "PAGADOSOSESESE", auth_number: 123123213123213654321, amount: "999380", comments: "Comentarios")
=> #<Payment id: nil, method: "DEPOSITOSITOSESES", status: "PAGADOSOSESESE", auth_number: "123123213123213654321", amount: 999380, comments: "Comentarios", created_at: nil, updated_at: nil, competitor_id: nil>
irb(main):008:0> pago_nuevo.save
   (0.2ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):009:0> pago_nuevo.errors.each do |k, v| p k.to_s + " " + v end
"competitor must exist"
"competitor_id can't be blank"
"method El método de pago es muy largo"
"status El estado del pago es muy largo"
"auth_number El número de autorización es muy largo"
"amount ¿De verdad están pagando tanto dinero?"
=> {:competitor=>["must exist"], :competitor_id=>["can't be blank"], :method=>["El método de pago es muy largo"], :status=>["El estado del pago es muy largo"], :auth_number=>["El número de autorización es muy largo"], :amount=>["¿De verdad están pagando tanto dinero?"]}
irb(main):010:0> 


irb(main):010:0> ciclist = Competitor.new()
=> #<Competitor id: nil, number: nil, first_lname: nil, second_lname: nil, names: nil, b_day: nil, gender: nil, e_mail: nil, blood_type: nil, phone: nil, allergies: nil, emergency_c_name: nil, emergency_c_phone: nil, team: nil, adult_tickets: nil, children_tickets: nil, amount_extra_tickets: nil, route_to_road: nil, responsive_sent: nil, responsive_received: nil, signature: nil, store_id: nil, hometown_id: nil, kind_id: nil, size_id: nil, created_at: nil, updated_at: nil>
irb(main):011:0> ciclist.save
   (0.2ms)  begin transaction
   (0.1ms)  rollback transaction
=> false
irb(main):012:0> ciclist.errors.each do |k, v| p k.to_s + " " + v end
"hometown must exist"
"kind must exist"
"size must exist"
"store must exist"
"number can't be blank"
"first_lname can't be blank"
"first_lname El primer apellido debe tener al menos un caracter"
"names can't be blank"
"names El nombre debe tener al menos un caracter"
"b_day can't be blank"
"b_day La fecha de nacimiento debe tener el formato: AAAAMMDD"
"b_day Debe ser numérico"
"gender can't be blank"
"gender En género por favor llene 'F' para Femenino y 'M' para Masculino"
"e_mail can't be blank"
"e_mail is invalid"
"e_mail El correo-e es muy corto, almenos debe tener a@a.a"
"phone can't be blank"
"phone El teléfono debe ser a 10 dígitos"
"phone Debe ser numérico"
"emergency_c_name can't be blank"
"emergency_c_name El nombre de contacto de emergencia es muy corto"
"emergency_c_phone can't be blank"
"emergency_c_phone El teléfono debe ser a 10 dígitos"
"emergency_c_phone Debe ser numérico"
"store_id can't be blank"
"hometown_id can't be blank"
"kind_id can't be blank"
=> {:hometown=>["must exist"], :kind=>["must exist"], :size=>["must exist"], :store=>["must exist"], :number=>["can't be blank"], :first_lname=>["can't be blank", "El primer apellido debe tener al menos un caracter"], :names=>["can't be blank", "El nombre debe tener al menos un caracter"], :b_day=>["can't be blank", "La fecha de nacimiento debe tener el formato: AAAAMMDD", "Debe ser numérico"], :gender=>["can't be blank", "En género por favor llene 'F' para Femenino y 'M' para Masculino"], :e_mail=>["can't be blank", "is invalid", "El correo-e es muy corto, almenos debe tener a@a.a"], :phone=>["can't be blank", "El teléfono debe ser a 10 dígitos", "Debe ser numérico"], :emergency_c_name=>["can't be blank", "El nombre de contacto de emergencia es muy corto"], :emergency_c_phone=>["can't be blank", "El teléfono debe ser a 10 dígitos", "Debe ser numérico"], :store_id=>["can't be blank"], :hometown_id=>["can't be blank"], :kind_id=>["can't be blank"]}
irb(main):013:0> 


