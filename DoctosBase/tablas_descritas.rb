####################################################################################################
# Tabla STORES - TIENDAS
####################################################################################################
name                                - Nombre de Tienda
location                            - Ubicación de la Tienda
b_register                          - ¿Registra participantes?
b_kits                              - ¿Entrega kits?
contact_name                        - Nombre de contacto
contact_phone                       - Teléfono(s) de contacto
contact_cell                        - Celular de contacto
contact_mail                        - Correo-e de contacto
web_page                            - Página de la tienda
fb_page                             - FaceBook de la tienda

####################################################################################################
# Tabla COMPETITORS - PARTICIPANTES
####################################################################################################
number                              - numero - Número de registro
first_lname                         - Apellido 1
second_lname                        - Apellido 2
names                               - Nombre (s)
b_day                               - Fecha de Nacimiento
gender                              - Sexo/Género
e_mail                              - Correo electrónico
blood_type                          - Tipo de sangre
phone                               - Teléfono
allergies                           - Alergias
emergency_c_name                    - Nombre contacto emergencia
emergency_c_phone                   - Teléfono de contacto de emergencia
team                                - Club/Equipo
adult_tickets                       - Boletos extra ADULTOS $23.00
children_tickets                    - Boletos extra NIÑOS $16.00
amount_extra_tickets                - $$$ Monto de Boletos Extra
route_to_road                       - Ruta en la que participará (Guacamayitas 45 km/Guacamayas Voladoras 55 km)
responsive_sent                     - Responsiva enviada
responsive_received                 - Responsiva recibida
signature                           - Firma
####
store_id                            - FK - Tienda que registra
hometown_id                         - Ciudad de Origen
kind_id                             - Tipo de persona (participante, staff, etc)
size_id                             - Talla Jersey (XCh, Ch, M, G, XG, XXG)
payment_id                          - Información del pago
#### Se debe quitar el store_id y guardar recorder_id. La persona que registra tiene ligada la
#### tienda a la que pertenece
user_id                             - FK - Persona que registra 

####################################################################################################
# Tabla PAYMENTS - PAGOS
####################################################################################################
payment_method                      - Forma de Pago
payment_status                      - Status de Pago
payment_id                          - Número de autorización
payment_amount                      - Monto de confirmado
payment_comments                    - Comentarios (P.E. "Recoger el pago en tienda")

####################################################################################################
# Tabla HOMETOWNS - CIUDADES DE ORIGEN
####################################################################################################
state_city_key                      - Clave de Estado-Ciudad (01001,01002,02001,02002,02003,03001,etc)
state_name                          - Nombre del Estado
city_name                           - Nombre de la ciudad

####################################################################################################
# Tabla KINDS - TIPOS DE PERSONA REGISTRADA
####################################################################################################
description                         - Descripción del tipo de persona (participante, staff, etc)

####################################################################################################
# Tabla SIZES - TALLAS DE JERSEYS
####################################################################################################
size                                - Talla Jersey (XCh, Ch, M, G, XG, XXG)

####################################################################################################
# Tabla USERS - PERSONAS/ENTIDADES QUE REGISTRAN
####################################################################################################
user                                - Usuario (único)
password                            - Contraseña (digerida)
e_mail                              - Correo del registrador, puede ser diferente al de la Tienda
name                                - Nombre de la persona que registra (puede ser el nombre de la tienda)
permission                          - 1 = Visualizar, 2 = Registrar, 3 = Actualizar, 4 = Eliminar, 5 = Administrar usuarios
####
store_id                            - Tienda a la que pertenece el registrador, nosotros = PLR