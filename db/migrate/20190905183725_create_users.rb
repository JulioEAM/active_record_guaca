class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :user
      t.string :password_digest
      t.string :email, limit: 50
      t.string :name, limit: 50
      t.integer :permission
      t.references :store, foreign_key: true

      t.timestamps
    end
    add_index :users, :user, unique: true
  end
end
