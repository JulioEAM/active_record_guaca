class CreateKinds < ActiveRecord::Migration[5.2]
  def change
    create_table :kinds do |t|
      t.string :description, limit: 20

      t.timestamps
    end
  end
end
