class AddValuesToHometown < ActiveRecord::Migration[5.2]
  def change
    add_column :hometowns, :competitors_count, :integer, :default => 0
  end
end
