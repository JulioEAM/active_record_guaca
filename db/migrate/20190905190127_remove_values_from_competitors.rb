class RemoveValuesFromCompetitors < ActiveRecord::Migration[5.2]
  def change
    remove_reference :competitors, :store, foreign_key: true
  end
end
