class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :method, limit: 10
      t.string :status, limit: 10
      t.string :auth_number, limit: 20
      t.integer :amount, limit: 5
      t.string :comments

      t.timestamps
    end
  end
end
