class CreateHometowns < ActiveRecord::Migration[5.2]
  def change
    create_table :hometowns do |t|
      t.integer :state_city_key, limit: 6
      t.string :state_name, limit: 25
      t.string :city_name, limit: 25

      t.timestamps
    end
  end
end
