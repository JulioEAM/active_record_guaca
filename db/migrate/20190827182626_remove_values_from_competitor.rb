class RemoveValuesFromCompetitor < ActiveRecord::Migration[5.2]
  def change
    remove_reference :competitors, :payment, foreign_key: true
  end
end
