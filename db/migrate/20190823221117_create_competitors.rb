class CreateCompetitors < ActiveRecord::Migration[5.2]
  def change
    create_table :competitors do |t|
      t.integer :number, limit: 3
      t.string :first_lname, limit: 15
      t.string :second_lname, limit: 15
      t.string :names, limit: 25
      t.integer :b_day, limit: 8
      t.string :gender, limit: 1
      t.string :e_mail, limit: 50
      t.string :blood_type, limit: 3
      t.integer :phone, limit: 10
      t.string :allergies, limit: 50
      t.string :emergency_c_name, limit: 50
      t.integer :emergency_c_phone, limit: 10
      t.string :team, limit: 40
      t.integer :adult_tickets, limit: 2
      t.integer :children_tickets, limit: 2
      t.integer :amount_extra_tickets, limit: 4
      t.string :route_to_road, limit: 30
      t.boolean :responsive_sent
      t.boolean :responsive_received
      t.string :signature, limit: 255
      t.references :store, foreign_key: true
      t.references :hometown, foreign_key: true
      t.references :kind, foreign_key: true
      t.references :size, foreign_key: true

      t.timestamps
    end
  end
end
