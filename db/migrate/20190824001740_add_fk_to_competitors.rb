class AddFkToCompetitors < ActiveRecord::Migration[5.2]
  def change
    add_reference :competitors, :payment, foreign_key: true
  end
end
