class AddNewValuesToPayments < ActiveRecord::Migration[5.2]
  def change
    add_reference :payments, :competitor, foreign_key: true
  end
end
