class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name, limit: 50
      t.string :location, limit: 255
      t.boolean :b_register
      t.boolean :b_kits
      t.string :contact_name, limit: 50
      t.integer :contact_phone, limit: 10
      t.integer :contact_cell, limit: 10
      t.string :contact_mail, limit: 50
      t.string :web_page, limit: 50
      t.string :fb_page, limit: 100

      t.timestamps
    end
  end
end
