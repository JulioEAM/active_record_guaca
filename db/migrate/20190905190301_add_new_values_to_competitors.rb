class AddNewValuesToCompetitors < ActiveRecord::Migration[5.2]
  def change
    add_reference :competitors, :user, foreign_key: true
  end
end
