# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_05_190301) do

  create_table "competitors", force: :cascade do |t|
    t.integer "number", limit: 3
    t.string "first_lname", limit: 15
    t.string "second_lname", limit: 15
    t.string "names", limit: 25
    t.integer "b_day", limit: 8
    t.string "gender", limit: 1
    t.string "e_mail", limit: 50
    t.string "blood_type", limit: 3
    t.integer "phone", limit: 10
    t.string "allergies", limit: 50
    t.string "emergency_c_name", limit: 50
    t.integer "emergency_c_phone", limit: 10
    t.string "team", limit: 40
    t.integer "adult_tickets", limit: 2
    t.integer "children_tickets", limit: 2
    t.integer "amount_extra_tickets", limit: 4
    t.string "route_to_road", limit: 30
    t.boolean "responsive_sent"
    t.boolean "responsive_received"
    t.string "signature", limit: 255
    t.integer "hometown_id"
    t.integer "kind_id"
    t.integer "size_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["hometown_id"], name: "index_competitors_on_hometown_id"
    t.index ["kind_id"], name: "index_competitors_on_kind_id"
    t.index ["size_id"], name: "index_competitors_on_size_id"
    t.index ["user_id"], name: "index_competitors_on_user_id"
  end

  create_table "hometowns", force: :cascade do |t|
    t.integer "state_city_key", limit: 6
    t.string "state_name", limit: 25
    t.string "city_name", limit: 25
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "competitors_count", default: 0
  end

  create_table "kinds", force: :cascade do |t|
    t.string "description", limit: 20
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.string "method", limit: 10
    t.string "status", limit: 10
    t.string "auth_number", limit: 20
    t.integer "amount", limit: 5
    t.string "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "competitor_id"
    t.index ["competitor_id"], name: "index_payments_on_competitor_id"
  end

  create_table "sizes", force: :cascade do |t|
    t.string "size", limit: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string "name", limit: 50
    t.string "location", limit: 255
    t.boolean "b_register"
    t.boolean "b_kits"
    t.string "contact_name", limit: 50
    t.integer "contact_phone", limit: 10
    t.integer "contact_cell", limit: 10
    t.string "contact_mail", limit: 50
    t.string "web_page", limit: 50
    t.string "fb_page", limit: 100
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "user"
    t.string "password_digest"
    t.string "email", limit: 50
    t.string "name", limit: 50
    t.integer "permission"
    t.integer "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["store_id"], name: "index_users_on_store_id"
    t.index ["user"], name: "index_users_on_user", unique: true
  end

end
