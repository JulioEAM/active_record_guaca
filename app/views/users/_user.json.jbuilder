json.extract! user, :id, :user, :email, :name, :permission, :store_id, :created_at, :updated_at
json.url user_url(user, format: :json)
