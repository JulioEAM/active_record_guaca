class Payment < ApplicationRecord
  belongs_to :competitor

  validates_presence_of :competitor_id, :method, :status, :auth_number, :amount,
                         on: :create
  validates_length_of :method,
                       maximum: 10,
                       message: "El método de pago es muy largo"
  validates_length_of :status,
                       maximum: 10,
                       message: "El estado del pago es muy largo"
  validates_length_of :auth_number,
                       maximum: 20,
                       message: "El número de autorización es muy largo"
  validates_length_of :amount,
                       within: 1..5,
                       too_long: "¿De verdad están pagando tanto dinero?",
                       too_short: "¿Ingresaste el monto correcto del pago?"

  validates_numericality_of :amount,
                             message: "Debe ser numérico"
end
