class Hometown < ApplicationRecord
  has_many :competitors

  validates_presence_of :state_city_key, :state_name, :city_name,
                         on: :create
  validates_length_of :state_city_key,
                       within: 4..6,
                       too_long: "La clave de estado-ciudad no puede ser tan larga",
                       too_short: "La clave de estado-ciudad no es suficiente para representarla"
  validates_length_of :state_name,
                       maximum: 25,
                       message: "El nombre del estado es muy largo"
  validates_length_of :city_name,
                       maximum: 25,
                       message: "El nombre de la ciudad es muy largo"

  validates_numericality_of :state_city_key,
                             message: "Debe ser numérico"
end
