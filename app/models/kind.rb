class Kind < ApplicationRecord
  has_many :competitors

  validates_presence_of :description,
                         on: :create
  validates_length_of :description,
                       maximum: 20,
                       message: "La descripción es muy larga"
end
