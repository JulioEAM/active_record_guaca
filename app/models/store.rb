class Store < ApplicationRecord
  has_many :competitors

  validates_presence_of :name, :location, :b_register, :b_kits, :contact_name, :contact_phone,
                         on: :create

  validates_format_of :contact_mail,
                       with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
                       message: "El correo de contacto no tiene un formato válido"
  validates_length_of :name,
                       maximum: 50,
                       message: "El nombre de la tienda es muy largo"
  validates_length_of :location,
                       maximum: 255,
                       message: "La ubicación de la tienda es muy larga"
  validates_length_of :contact_name,
                       maximum: 50,
                       message: "El nombre de contacto es muy largo, máximo %d"
  validates_length_of :contact_phone,
                       maximum: 10,
                       message: "El teléfono de contacto es muy largo, máximo %d"
  validates_numericality_of :contact_phone,
                             message: "Debe ser numérico"
end
