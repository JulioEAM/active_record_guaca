class Size < ApplicationRecord
  has_many :competitors

  validates_presence_of :size,
                         on: :create
  validates_length_of :size,
                       maximum: 4,
                       message: "La talla es muy larga"
end
