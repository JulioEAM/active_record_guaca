class Competitor < ApplicationRecord
  belongs_to :hometown, :counter_cache => true
  belongs_to :kind
  has_many :payments
  belongs_to :size
  belongs_to :store

  validates_presence_of :number, :first_lname, :names, :b_day, :gender, :e_mail,
                        :phone, :emergency_c_name, :emergency_c_phone, :store_id, :hometown_id,
                        :kind_id,
                         on: :create

  validates_format_of :e_mail,
                       with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  validates_length_of :first_lname,
                       within: 1..15,
                       too_long: "Primer apellido muy largo",
                       too_short: "El primer apellido debe tener al menos un caracter"
  validates_length_of :names,
                       within: 1..15,
                       too_long: "Nombre muy largo",
                       too_short: "El nombre debe tener al menos un caracter"
  validates_length_of :b_day,
                       is: 8,
                       message: "La fecha de nacimiento debe tener el formato: AAAAMMDD"
  validates_length_of :gender,
                       is: 1,
                       message: "En género por favor llene 'F' para Femenino y 'M' para Masculino"
  validates_length_of :e_mail,
                       within: 5..50,
                       too_long: "El correo-e es muy largo",
                       too_short: "El correo-e es muy corto, almenos debe tener a@a.a"
  validates_length_of :phone, :emergency_c_phone,
                       is: 10,
                       message: "El teléfono debe ser a 10 dígitos"
  validates_length_of :emergency_c_name,
                       within: 5..50,
                       too_long: "El nombre de contacto de emergencia es muy largo",
                       too_short: "El nombre de contacto de emergencia es muy corto"

  validates_numericality_of :b_day, :phone, :emergency_c_phone,
                             message: "Debe ser numérico"
end
